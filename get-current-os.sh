#!/bin/bash

# exit on error
set -e

CURRDIR=$(pwd)
CURROPT=$CURRDIR/opt
CURRBIN=$CURRDIR/bin
CURRSRC=$CURRDIR/src
CURRDPOSSTC=$CURRDIR/DarkPhoenixOS_Specific_Toolchain
CURRMS=$CURRDIR/meaty-skeleton
CURRDPOS=$CURRDIR/DarkPhoenixOS

if [ -d "$CURRDPOSSTC" ]; then
  rm -rf $CURRDPOSSTC
fi

if [ -d "$CURRMS" ]; then
  rm -rf $CURRMS
fi

if [ -d "$CURRDPOS" ]; then
  if [ -L "$CURRDPOS" ]; then
    # It is a symlink!
    rm "$CURRDPOS"
  else
    # It's a directory!
    rm -rf $CURRDPOS
  fi
  
fi

git clone https://gitlab.com/DarkPhoenix6/DarkPhoenixOS_Specific_Toolchain.git
cd DarkPhoenixOS_Specific_Toolchain
git config --global credential.helper cache
git config credential.helper store

cd $CURRDIR

git clone https://gitlab.com/DarkPhoenix6/meaty-skeleton.git
cd meaty-skeleton
git config --global credential.helper cache
git config credential.helper store



cd $CURRDIR

ln -s meaty-skeleton DarkPhoenixOS
exit
# :)
