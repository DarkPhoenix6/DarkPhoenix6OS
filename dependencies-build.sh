#!/bin/bash

# exit on error
set -e

CURRDIR=$(pwd)
CURROPT=$CURRDIR/opt
CURRBIN=$CURRDIR/bin
CURRSRC=$CURRDIR/src
CURRDPOSSTC=$CURRDIR/DarkPhoenixOS_Specific_Toolchain
CURRMS=$CURRDIR/meaty-skeleton
CURRDPOS=$CURRDIR/DarkPhoenixOS

if [ ! -d "$CURRBIN" ]; then
  mkdir $CURRBIN
fi
if [ ! -d "$CURRSRC" ]; then
  mkdir $CURRSRC
fi
if [ ! -d "$CURROPT" ]; then
  mkdir $CURROPT
fi
cd $CURRSRC

tar -xf autoconf-2.64.tar.xz
cd autoconf-2.64
./configure --prefix=$CURROPT/autoconf-2.64
make
make install

cd $CURRSRC
tar -xf autoconf-2.68.tar.xz
cd autoconf-2.68
./configure --prefix=$CURROPT/autoconf-2.68
make
make install

PATH="$CURROPT/autoconf-2.68/bin:$PATH"
#cp auto* src/
#cd src
cd $CURRSRC
tar -xf automake-1.14.tar.xz
tar -xf automake-1.14.1.tar.xz
cd automake-1.14
mkdir build
cd build
../configure --prefix=$CURROPT/automake-1.14
make
make install
#sudo make install
cd $CURRSRC

cd automake-1.14.1
mkdir build
cd build
../configure --prefix=$CURROPT/automake-1.14.1
make
make install
#sudo make install

cd $CURRDIR

ln -s ${CURROPT}/automake-1.14.1 bin/automake-1.14.1
ln -s ${CURROPT}/autoconf-2.68 bin/autoconf-2.68
ln -s ${CURROPT}/autoconf-2.64 bin/autoconf-2.64
exit
# :)
