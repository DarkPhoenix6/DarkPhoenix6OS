#!/bin/bash

# exit on error
set -e
CURRDIR=$(pwd)
CURROPT=$CURRDIR/opt
CURRBIN=$CURRDIR/bin
CURRSRC=$CURRDIR/src
CURRDPOSSTC=$CURRDIR/DarkPhoenixOS_Specific_Toolchain
CURRMS=$CURRDIR/meaty-skeleton
CURRDPOS=$CURRDIR/DarkPhoenixOS

. ./clean.sh
. ./dependencies.sh
. ./dependencies-build.sh
. ./build-gcc.sh
. ./get-current-os.sh
. ./cross-compiler_build.sh
. ./set_path.sh
cd $CURRDPOS
. ./build.sh
cd $CURRDIR
. ./reset_symlinks.sh
. ./Build-OS-specific-toolchain.sh


cd $CURRDIR
exit
# :)
