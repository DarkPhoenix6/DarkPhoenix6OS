#!/bin/bash

# exit on error
set -e


CURRDIR=$(pwd)
CURROPT=$CURRDIR/opt
CURRSRC=$CURRDIR/src
CURRBIN=$CURRDIR/bin
CURRDPOSSTC=$CURRDIR/DarkPhoenixOS_Specific_Toolchain
CURRMS=$CURRDIR/meaty-skeleton
CURRDPOS=$CURRDIR/DarkPhoenixOS

if [ ! -d "$CURROPT" ]; then
  mkdir $CURROPT
fi

export PREFIX="$CURROPT/gcc-7.3.0"


cd $CURRSRC
if [ ! -d "binutils-2.30" ]; then
  tar -xf binutils-2.30.tar.xz
fi
if [ ! -d "gcc-7.3.0" ]; then
  tar -xf gcc-7.3.0.tar.xz
fi

if [ ! -d "build-binutils" ]; then
  mkdir build-binutils
fi
cd build-binutils
../binutils-2.30/configure --prefix="$PREFIX" --disable-nls --disable-werror
make
make install

cd $CURRSRC

if [ ! -d "build-gcc" ]; then
  mkdir build-gcc
fi

cd gcc-7.3.0
. contrib/download_prerequisites
cd ../build-gcc
sed -i 's@\./fixinc\.sh@-c true@' ../gcc-7.3.0/gcc/Makefile.in
../gcc-7.3.0/configure --prefix="$PREFIX" --disable-nls --enable-languages=c,c++ --enable-multilib
#../gcc-7.3.0/configure --prefix="$PREFIX" --disable-nls --enable-languages=c,c++
make
make install


cd $CURRSRC
ln -s opt/gcc-7.3.0 bin/gcc-7.3.0

exit
# :)
