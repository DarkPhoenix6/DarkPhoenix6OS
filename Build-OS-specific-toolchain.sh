#!/bin/bash

# exit on error
set -e

function delete_dir_check {

if [ -d "$1" ]; then
  if [ -L "$1" ]; then
    # It is a symlink!
    rm "$1"
  else
    # It's a directory!
    rm -rf $1
  fi
fi

}

CURRDIR=$(pwd)
CURROPT=$CURRDIR/opt
CURRBIN=$CURRDIR/bin
CURRSRC=$CURRDIR/src
CURRDPOSSTC=$CURRDIR/DarkPhoenixOS_Specific_Toolchain
CURRMS=$CURRDIR/meaty-skeleton
CURRDPOS=$CURRDIR/DarkPhoenixOS
export SYSROOT=${CURROPT}/darkphoenixos

 
# make symlinks (a bad hack) to make newlib work
cd CURROPT/cross/bin/ # this is where the bootstrapped generic cross compiler toolchain (i686-elf-xxx) is installed in,
                # change this based on your development environment.
ln i686-elf-ar i686-darkphoenixos-ar
ln i686-elf-as i686-darkphoenixos-as
ln i686-elf-gcc i686-darkphoenixos-gcc
ln i686-elf-gcc i686-darkphoenixos-cc
ln i686-elf-ranlib i686-darkphoenixos-ranlib
ln i686-elf-c++ i686-darkphoenixos-c++
ln i686-elf-g++  i686-darkphoenixos-g++
ln i686-elf-ld i686-darkphoenixos-ld

# return
cd $CURRDIR

export TARGET=i686-darkphoenixos

mkdir build-newlib
cd build-newlib
../newlib-3.0.0.20180226/configure --prefix=/usr --target=i686-darkphoenixos
make all
make DESTDIR=${CURROPT}/darkphoenixos install
#make DESTDIR=${SYSROOT} install
#cp -ar $SYSROOT/usr/i386-myos/* $SYSROOT/usr/
#cp -ar ${HOME}/opt/darkphoenixos/usr/i386-darkphoenixos/* ${HOME}/opt/darkphoenixos/usr/

cp -ar ${CURROPT}/darkphoenixos/usr/i686-darkphoenixos/* ${CURROPT}/darkphoenixos/usr/

rm $CURROPT/cross/bin/i686-darkphoenixos*
export PREFIX="$CURROPT/cross"
SYSROOT=${CURROPT}/darkphoenixos
export TARGET=i686-darkphoenixos
mkdir build-binutils
cd build-binutils
../binutils-2.30/configure --target=$TARGET --prefix="$PREFIX" --with-sysroot=$SYSROOT --disable-nls --disable-werror
make
make install

cd ../
cd build-gcc
../gcc-7.3.0/configure --target=$TARGET --prefix="$PREFIX" --with-sysroot=$SYSROOT --disable-nls --enable-languages=c,c++
make all-gcc
make all-target-libgcc
make install-gcc
make install-target-libgcc

ulimit -s 32768

: << '--MULTILINE-COMMENT--'
cd build-newlib
../newlib-3.0.0.20180226/configure --target=$TARGET --prefix="$PREFIX" --with-sysroot=$SYSROOT
make all
make install

make DESTDIR=${CURROPT}/cross install
cp -ar ${CURROPT}/cross/usr/i686-darkphoenixos/* ${CURROPT}/cross/usr/
--MULTILINE-COMMENT--
exit
# :)
