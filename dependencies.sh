#!/bin/bash

# exit on error
set -e

CURRDIR=$(pwd)
CURROPT=$CURRDIR/opt
CURRSRC=$CURRDIR/src
CURRDPOSSTC=$CURRDIR/DarkPhoenixOS_Specific_Toolchain
CURRMS=$CURRDIR/meaty-skeleton
CURRDPOS=$CURRDIR/DarkPhoenixOS

#if debian 9
sudo apt-get install gcc g++ make bison flex gawk libgmp3-dev libmpfr-dev libmpfr-doc libmpfr4 libmpfr4-dbg build-essential gcc-multilib libc6-i386 mpc xorriso git

if [ ! -d "$CURRSRC" ]; then
  mkdir $CURRSRC
fi


cd $CURRSRC
wget https://ftp.gnu.org/gnu/automake/automake-1.14.tar.xz
wget https://ftp.gnu.org/gnu/automake/automake-1.14.1.tar.xz
wget https://ftp.gnu.org/gnu/glibc/glibc-2.27.tar.xz
wget ftp://sourceware.org/pub/newlib/newlib-3.0.0.20180226.tar.gz
wget ftp://ftp.gnu.org/gnu/grub/grub-2.02.tar.xz
#wget https://gmplib.org/download/gmp/gmp-6.1.2.tar.lz
#wget http://isl.gforge.inria.fr/isl-0.19.tar.xz
#wget http://www.mpfr.org/mpfr-current/#download
#wget https://ftp.gnu.org/gnu/mpc/mpc-1.1.0.tar.gz
wget https://ftp.gnu.org/gnu/texinfo/texinfo-6.5.tar.xz
wget https://ftp.gnu.org/gnu/gcc/gcc-7.3.0/gcc-7.3.0.tar.xz
wget http://gnu.mirror.globo.tech/autoconf/autoconf-2.64.tar.xz
wget http://gnu.mirror.globo.tech/automake/automake-1.11.6.tar.xz
wget http://gnu.mirror.globo.tech/autoconf/autoconf-2.68.tar.xz
wget https://ftp.gnu.org/gnu/binutils/binutils-2.30.tar.xz
wget https://www.nasm.us/pub/nasm/releasebuilds/2.13.03/nasm-2.13.03.tar.xz

cd $CURRDIR

exit
# :)
