#!/bin/bash

# exit on error
set -e


CURRDIR=$(pwd)
CURROPT=$CURRDIR/opt
CURRBIN=$CURRDIR/bin
CURRSRC=$CURRDIR/src
CURRDPOSSTC=$CURRDIR/DarkPhoenixOS_Specific_Toolchain
CURRMS=$CURRDIR/meaty-skeleton
CURRDPOS=$CURRDIR/DarkPhoenixOS

export PATH="$CURRBIN:$CURROPT/cross/bin:$CURROPT/gcc-7.3.0/bin:$PATH"


exit
# :)
