#!/bin/bash

# exit on error
set -e


CURRDIR=$(pwd)
CURROPT=$CURRDIR/opt
CURRSRC=$CURRDIR/src
CURRDPOSSTC=$CURRDIR/DarkPhoenixOS_Specific_Toolchain
CURRMS=$CURRDIR/meaty-skeleton
CURRDPOS=$CURRDIR/DarkPhoenixOS

if [ ! -d "$CURROPT" ]; then
  mkdir $CURROPT
fi

export MYGCC="$CURROPT/gcc-7.3.0/"
export PATH="$CURROPT/gcc-7.3.0/bin:$PATH"
export PREFIX="$CURROPT/cross"
export TARGET=i686-elf
export PATH="$PREFIX/bin:$PATH"


cd $CURRSRC
if [ -d "build-gcc" ]; then
  mv  build-gcc build-gcc.bak
  rm -rf build-gcc
fi
if [ -d "build-binutils" ]; then
  mv build-binutils build-binutils.bak
  rm -rf build-binutils
fi

mkdir build-binutils
cd build-binutils
../binutils-2.30/configure --target=$TARGET --prefix="$PREFIX" --with-sysroot --disable-nls --disable-werror
make
make install

cd $CURRSRC
 
# The $PREFIX/bin dir _must_ be in the PATH. We did that above.
which -- $TARGET-as || echo $TARGET-as is not in the PATH
 
mkdir build-gcc
cd build-gcc
sed -i 's@\./fixinc\.sh@-c true@' ../gcc-7.3.0/gcc/Makefile.in
../gcc-7.3.0/configure --target=$TARGET --prefix="$PREFIX" --disable-nls --enable-languages=c,c++ --without-headers
make all-gcc
make all-target-libgcc
make install-gcc
make install-target-libgcc

#. ./cross-test.sh

#export PATH="$CURROPT/gcc-7.3.0/bin:$PATH"
#export PATH="$CURROPT/cross/bin:$CURROPT/gcc-7.3.0/bin:$PATH"

cd $CURRDIR

exit
# :)
