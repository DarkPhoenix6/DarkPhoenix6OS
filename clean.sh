#!/bin/bash

# exit on error
set -e


function delete_dir_check {

if [ -d "$1" ]; then
  if [ -L "$1" ]; then
    # It is a symlink!
    rm "$1"
  else
    # It's a directory!
    rm -rf $1
  fi
fi

}

CURRDIR=$(pwd)
CURROPT=$CURRDIR/opt
CURRBIN=$CURRDIR/bin
CURRSRC=$CURRDIR/src
CURRDPOSSTC=$CURRDIR/DarkPhoenixOS_Specific_Toolchain
CURRMS=$CURRDIR/meaty-skeleton
CURRDPOS=$CURRDIR/DarkPhoenixOS
if [ -d "$CURROPT" ]; then
  rm -rf $CURROPT/*
fi

if [ -d "$CURRSRC" ]; then
  rm -rf $CURRSRC
fi

if [ -d "$CURRBIN" ]; then
  rm -rf $CURRBIN/*
fi

if [ -d "$CURRDPOSSTC" ]; then
  rm -rf $CURRDPOSSTC
fi

if [ -d "$CURRMS" ]; then
  rm -rf $CURRMS
fi

if [ -d "$CURRDPOS" ]; then
  if [ -L "$CURRDPOS" ]; then
    # It is a symlink!
    rm "$CURRDPOS"
  else
    # It's a directory!
    rm -rf $CURRDPOS
  fi
  
fi

exit
# :)
