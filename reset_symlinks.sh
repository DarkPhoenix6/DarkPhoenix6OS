#!/bin/bash

# exit on error
set -e


function delete_dir_check {

if [ -d "$1" ]; then
  if [ -L "$1" ]; then
    # It is a symlink!
    #echo "$1 is a symlink"
    rm "$1"
  else
    # It's a directory!
    #echo "$1 is a dir"
    rm -rf $1
  fi
fi

}
function create_symlink_from_dir {
if [ -d "$1" ]; then
  ln -s "$1" "$2"
else
  echo "src dir does not exist!"
fi
}

CURRDIR=$(pwd)
CURROPT=$CURRDIR/opt
CURRSRC=$CURRDIR/src
CURRBIN=$CURRDIR/bin
CURRDPOSSTC=$CURRDIR/DarkPhoenixOS_Specific_Toolchain
CURRMS=$CURRDIR/meaty-skeleton
CURRDPOS=$CURRDIR/DarkPhoenixOS

if [ ! -d "$CURRBIN" ]; then
  mkdir $CURRBIN
fi

if [ ! -d "$CURROPT" ]; then
  echo "error: please build first!!!"
  exit
fi
delete_dir_check "${CURRBIN}/gcc-7.3.0"
delete_dir_check "${CURRBIN}/automake-1.14.1"
delete_dir_check "${CURRBIN}/autoconf-2.68"
delete_dir_check "${CURRBIN}/autoconf-2.64"

function no_use {
create_symlink_from_dir "${CURROPT}/gcc-7.3.0" "bin/gcc-7.3.0"
create_symlink_from_dir "${CURROPT}/automake-1.14.1" "bin/automake-1.14.1"
create_symlink_from_dir ${CURROPT}/autoconf-2.68 bin/autoconf-2.68
create_symlink_from_dir ${CURROPT}/autoconf-2.64 bin/autoconf-2.64
}

cd $CURRBIN
create_symlink_from_dir "../opt/gcc-7.3.0" "gcc-7.3.0"
create_symlink_from_dir "../opt/automake-1.14.1" "automake-1.14.1"
create_symlink_from_dir "../opt/autoconf-2.68" "autoconf-2.68"
create_symlink_from_dir "../opt/autoconf-2.64" "autoconf-2.64"
exit
# :)
